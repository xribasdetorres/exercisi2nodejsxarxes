const fs = require('fs');
var formatdata = require("./Moduls/inputManager");
var ComandLines = new Array();
var Names =  new Array();
var Power = new Array();
var Type = new Array();
var numberFirst = 0;
var numberSecond = 0;
var number3 = 0;
var number4 = 0;

console.log("Welcome to the player list manager.");
console.log("If do you dont know how to use tipe 'HELP'");

interactiveConsole();











function interactiveConsole()
{
    input(">> ", function(data){
            
           ComandLines = formatdata.getCommandLines(data);
            
            if(ComandLines[0] == "EXIT")
            {
                
                console.log("Good bye");
                process.exit(1);
            }
            else if(ComandLines[0] == "ADD")
            {
                if(findname(ComandLines[1]) == false)
                {
                        Names.push(ComandLines[1]);
                        Power.push(ComandLines[2]);
                        Type.push(ComandLines[3]);
                        console.log("Player added correctly");
                        interactiveConsole();
                }else
                {
                    console.log("Name ready exist. Please put another name:");
                    interactiveConsole();
                }
            }
            else if(ComandLines[0] == "LIST")
            {
                printPlayers();
                interactiveConsole();
            }
            else if(ComandLines[0] == "DELETE")
            {
                deletePlayer(ComandLines[1]);
                interactiveConsole();
            }
            else if(ComandLines[0] == "LOAD")
            {
                loadFile(ComandLines[1]);
                interactiveConsole();
                
            }
            else if(ComandLines[0] == "SAVE")
            {
                saveFile(ComandLines[1]);
                interactiveConsole();

            }
            else if(ComandLines[0] == "HELP")
            {
                console.log("These are the commands:");
                console.log("ADD [NAME] [POWER] [TYPE]  >>  to add a character");
                console.log("DELETE [NAME] >> to delete a player");
                console.log("LIST >> To see all the players added");
                console.log("SAVE [NAME] >> for save the players list in a JSON file");
                console.log("LOAD [NAME] >> for load the JSON file");
                interactiveConsole();
            }
            else{
                console.log("Command not found");
                console.log("Please use 'HELP' for help")
                interactiveConsole();
            }
    })
}

function input(question, callback)
{
    var stdin = process.stdin, stdout = process.stdout;

    stdin.resume();
    stdout.write(question);

    stdin.once('data', function(data)
        {
            data = data.toString().trim();
            callback(data);
        }
    )
}

function deletePlayer(name)
{
    var indexplayer =  0;
    if(findname(name))
    {
      indexplayer =  Names.indexOf(name);
      Names.splice(indexplayer, 1);
      Power.splice(indexplayer, 1); 
      Type.splice(indexplayer, 1);
      console.log("Player correctly deleted");
    }
    else
    {
        console.log("Player not found. Please select another name.");
        
    }
}

function findname(name)
{
    var sameName = 0;
    var issamesnames = false;
    Names.forEach(
        function(value)
        {
            if(value == name)
            {
                sameName++;
            }
        }
    );
    if(sameName > 0)
    {
        issamesnames = true;
    }
    return issamesnames;
}
function printPlayers()
{
    console.log("Name       Power       Type");
    var indexPlayer = 0
    while( Names[indexPlayer] != null)
    {
       console.log(Names[indexPlayer ] + "      " + Power[indexPlayer] + "        " + Type[indexPlayer]);   
       indexPlayer++;
    }
}
function saveFile(name)
{
    var indexArray = 0;
    var indexArray2 = 0;
    let players = new Array();
    var data = new Array();
    let list = new Array();
    while(Names[indexArray] != null)
    {
        players[indexArray] = 
        {
            name: Names[indexArray],
            power: Power[indexArray],
            type: Type[indexArray]
        }
    
        
        
        
        indexArray++;
        
    };
    
    data = JSON.stringify(players,  null, 2);
    
    fs.writeFileSync('./listsplayers/' + name + '.json', data);
}
function loadFile(namefile)
{
    
        var file = 'listsplayers/' + namefile + '.json';
        fs.readFile(file, (err,data) =>
        {
           if(err == null){
            var indexJSON = 0; 
            var players = JSON.parse(data);
            for(var player in players)
             {
                
                Names[player]=(players[player].name);
                Power[player]=(players[player].power);
                Type[player]=(players[player].type);
             }
            }
            else
            {
                console.log("File not found");
            }
        });


    
}